var texts = {
  font: null,
  desc: 'There are far more activities and opportunities in the world than we have time and resources to invest in and although many of them may be good, or even very good, the fact is that most are trivial and few are vital.',
  pool: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
  names: null,
  shop: null,
  read: ['news', 'books', 'articles'],
  notification: ['Meeting with ', 'Message by ', 'Incoming call by ', 'Meeting at ', 'Buy ', 'Read ', 'Email by ']
}

var colors = {
  black: [0, 0, 0],
  white: [255, 255, 255]
}

var board = {
  x: null,
  y: null
}

var states = {
  speed: [],
  posY: [],
  posX: [],
  img: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
}

var sound = []
var images = []

function preload() {
  texts.font = loadFont('ttf/castoro.ttf')
  texts.names = loadStrings('txt/names.txt')
  texts.shop = loadStrings('txt/shopping.txt')
  for (var i = 0; i < 2; i++) {
    sound.push(loadSound('mp3/noty' + i + '.mp3'))
  }
  images.push(loadImage('img/meet.png'))
  images.push(loadImage('img/mess.png'))
  images.push(loadImage('img/call.png'))
  images.push(loadImage('img/time.png'))
  images.push(loadImage('img/shop.png'))
  images.push(loadImage('img/read.png'))
  images.push(loadImage('img/mail.png'))
}

function setup() {
  if (windowWidth >= windowHeight * 2) {
    board.y = windowHeight * 0.95
    board.x = board.y * 2
  } else {
    board.x = windowWidth * 0.95
    board.y = board.x * (1 / 2)
  }
  for (var i = 0; i < texts.pool.length; i++) {
    states.speed.push(Math.random() * 0.25 + 0.25)
    states.posX.push(Math.random())
    states.posY.push(-Math.random() * 0.5)
    var temp = Math.floor(Math.random() * texts.notification.length)
    if (temp === 0) {
      texts.pool[i] = texts.notification[0] + texts.names[Math.floor(Math.random() * (texts.names.length - 1))]
    } else if (temp === 1) {
      texts.pool[i] = texts.notification[1] + texts.names[Math.floor(Math.random() * (texts.names.length - 1))]
    } else if (temp === 2) {
      texts.pool[i] = texts.notification[2] + texts.names[Math.floor(Math.random() * (texts.names.length - 1))]
    } else if (temp === 3) {
      texts.pool[i] = texts.notification[3] + Math.floor(Math.random() * 13) + ':' + Math.floor(Math.random() * 13) * 5
    } else if (temp === 4) {
      texts.pool[i] = texts.notification[4] + texts.shop[Math.floor(Math.random() *( texts.shop.length - 1))]
    } else if (temp === 5) {
      texts.pool[i] = texts.notification[5] + texts.read[Math.floor(Math.random() * texts.read.length)]
    } else if (temp === 6) {
      texts.pool[i] = texts.notification[6] + texts.names[Math.floor(Math.random() * (texts.names.length - 1))]
    }
    states.img[i] = temp
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  background(255)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  textAlign(LEFT, CENTER)
  textFont(texts.font)

  fill(colors.black)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, board.x * 0.3, board.y * 0.3)

  textSize(board.x * 0.015)
  fill(colors.white)
  noStroke()
  text(texts.desc, windowWidth * 0.5, windowHeight * 0.5, board.x * 0.26, board.y * 0.25)

  for (var i = 0; i < texts.pool.length; i++) {
    states.posY[i] += deltaTime * Math.floor(states.speed[i] * 100) * 0.01 * 0.0001
    if (states.posY[i] >= 1.1) {
      states.posY[i] = -Math.random() * 0.5
      states.posX[i] = Math.random()
      states.speed[i] = Math.random() * 0.25 + 0.25
      var temp = Math.floor(Math.random() * texts.notification.length)
      if (temp === 0) {
        texts.pool[i] = texts.notification[0] + texts.names[Math.floor(Math.random() * (texts.names.length - 1))]
      } else if (temp === 1) {
        texts.pool[i] = texts.notification[1] + texts.names[Math.floor(Math.random() * (texts.names.length - 1))]
      } else if (temp === 2) {
        texts.pool[i] = texts.notification[2] + texts.names[Math.floor(Math.random() * (texts.names.length - 1))]
      } else if (temp === 3) {
        texts.pool[i] = texts.notification[3] + Math.floor(Math.random() * 13) + ':' + Math.floor(Math.random() * 12) * 5
      } else if (temp === 4) {
        texts.pool[i] = texts.notification[4] + texts.shop[Math.floor(Math.random() *( texts.shop.length - 1))]
      } else if (temp === 5) {
        texts.pool[i] = texts.notification[5] + texts.read[Math.floor(Math.random() * texts.read.length)]
      } else if (temp === 6) {
        texts.pool[i] = texts.notification[6] + texts.names[Math.floor(Math.random() * (texts.names.length - 1))]
      }
    }
    if (Math.floor(states.posY[i] * windowHeight) === Math.floor(board.y * 0.1)) {
      sound[Math.floor(Math.random() * 2)].play()
    }
    drawToDo(states.posX[i], states.posY[i], texts.pool[i], states.img[i])
  }
}

function drawToDo(posX, posY, content, img) {
  rectMode(CORNER)
  fill(colors.white)
  noStroke()
  drawingContext.shadowOffsetX = 5
  drawingContext.shadowOffsetY = 5
  drawingContext.shadowBlur = 10
  drawingContext.shadowColor = 'gray'
  rect(windowWidth * 0.5 - board.x * 0.5 + board.x * posX, windowHeight - posY * windowHeight, textWidth(content) + board.x * 0.12, board.y * 0.1, board.x * 0.15, board.x * 0.15, 0, board.x * 0.15)

  textAlign(LEFT, CENTER)
  textSize(board.x * 0.02)
  fill(colors.black)
  noStroke()
  text(content, windowWidth * 0.5 - board.x * 0.5 + board.x * 0.06 + board.x * posX, windowHeight - posY * windowHeight, textWidth(content) + board.x * 0.12, board.y * 0.1)

  for (var j = 0; j < texts.notification.length; j++) {
    if (img === j) {
        image(images[j], windowWidth * 0.5 - board.x * 0.5 + board.x * 0.02 + board.x * posX, windowHeight - posY * windowHeight + board.x * 0.0125, board.x * 0.025, board.x * 0.025)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight * 2) {
    board.y = windowHeight * 0.95
    board.x = board.y * 2
  } else {
    board.x = windowWidth * 0.95
    board.y = board.x * (1 / 2)
  }
  createCanvas(windowWidth, windowHeight)
}
